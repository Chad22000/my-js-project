function get_temp(lat, lon){

  $.getJSON("https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=7d583ca2746cba020eb0868cf4022d3e", function(data){
    var icon = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";
    var temp = data.main.temp-273.15;
    var weather = data.weather[0].main;

    $('.icon').attr('src', icon);
    $('.temp').text(temp);
    $('.weather').text(weather);
  });
}

$.getJSON("https://opensky-network.org/api/states/all", function(data){
  console.log(data);

  var myParent = document.querySelector("#selecteur");

//Create array of options to be added
  let array = data["states"]

//Create and append select list
  var selectList = document.createElement("select");
  selectList.id = "mySelect";
  myParent.appendChild(selectList);

//Create and append the options
  for (var i = 0; i < array.length; i++) {
    var option = document.createElement("option");
    option.value = array[i];
    option.text = "Départ: "+array[i][2]+" / N°"+array[i][1];
    selectList.appendChild(option);
  }
  const selectElement = document.querySelector('#mySelect');
  selectElement.addEventListener('change', function () {
    let data = this.value.split(",")
    get_temp(data[6],data[5]);

  })
  const button = document.querySelector('#button');
  button.addEventListener("click", function() {
    let data = document.querySelector('#mySelect').value.split(",")
    fly_num = data[1]
    lat_cor = data[5]
    lon_cor = data[6]
    weather = document.querySelector('#weather').textContent
    $.ajax({
      type: 'POST',
      url: 'php/script.php',
      data: {fly_num: fly_num, lat_cor: lat_cor, lon_cor: lon_cor, weather: weather}
    })
  })
})


$.ajax({
  type: "POST",
  url: "php/get-data.php",
  success: function(data){
    console.log(data)
    const div = document.getElementById("database_div")
    div.innerHTML = data
    //echo what the server sent back...
  }
});

/*
  AUTRE METHODE
//Code JavaScript
var req = new XMLHttpRequest();
req.onload = function() {
  console.log(this.responseText);
};
req.open("get", "php/get-data.php", true);
req.send();

document.getElementById("database_dib"). = req.responseText;
*/
