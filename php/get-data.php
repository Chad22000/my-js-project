<?php
// on se connecte à MySQL
function build_table($array)
{
// data rows
  $html = "";
  foreach ($array as $key => $value) {
    $html .= '<td>' . htmlspecialchars($value) . '</td>';
  }
  return $html;

// finish table and return it


}

$servername = "localhost";
$username = "root";
$password = "";
$database = "weather_fly";


// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

// on crée la requête SQL


$data = $conn->query("SELECT * FROM `data`");

if ($data->num_rows > 0) {
  // output data of each row
  echo '<table>';
  while ($row = $data->fetch_object()) {
    echo "<tr>";
    echo build_table($row);
    echo "</tr>";
  }
  echo '</table>';


} else {
  echo "0 results";
}

$conn->close();

?>
